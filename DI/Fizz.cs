﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StructureMap;

namespace FizzBuzz.DI
{
    public class Fizz: IFizzbuzzRule
    {
        public bool Evaluate(int i)
        {
            return (i % 3 == 0);
        }
        public string getResult()
        {
            return "Fizz";
        }
    }
}
