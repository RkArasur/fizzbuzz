﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.DI
{
    public class Buzz:IFizzbuzzRule
    {
        public bool Evaluate(int i)
        {
            return (i % 5 == 0);
        }
        public string getResult()
        {
            return "buzz";
        }
    }
}
