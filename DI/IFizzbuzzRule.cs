﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.DI
{
    interface IFizzbuzzRule
    {
        public bool Evaluate(int x);
        public string getResult();
    }
}
