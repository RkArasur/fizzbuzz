﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Models
{
    public class FizzbuzzViewModel
    {
        public int number { get; set; }
        public List<string> Fizzbuzznumbers { get; set; }
    }
}
