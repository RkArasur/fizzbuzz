﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StructureMap;
using FizzBuzz.DI;

namespace FizzBuzz.Models
{
    public class MyFizzbuzz
    {
        public List<string> GetFizzBuzz(int value)
        {
            var numbers = new List<string>();
            for (var i = 1; i <= value; i++)
            {
                numbers.Add(GetFizzBuzzValue(i));
            }
            return numbers;
        }

        public string GetFizzBuzzValue(int val)
        {
            
        }
    }
}
